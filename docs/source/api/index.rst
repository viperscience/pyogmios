API
===

.. toctree::
   :maxdepth: 2

   client
   datatypes
   chainsync
   mempool
   statequery
   txsubmit