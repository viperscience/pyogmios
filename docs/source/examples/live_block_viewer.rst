Live Block Viewer
===================================

This module prints new blocks as they are added to the blockchain: ::

    import time
    import ogmios

    with ogmios.Client() as client:
        # Set chain pointer to origin
        point, tip, id = client.find_intersection.execute([ogmios.Origin()])

        # Now set chain pointer to tip
        _, _, _ = client.find_intersection.execute([tip.to_point()])

        # Tail blockchain as new blocks come in beyond the current tip
        while True:
            direction, tip, point, _ = client.next_block.execute()
            if direction == ogmios.Direction.forward:
                print(f"New block: {point}")
            time.sleep(1)

Example output:

.. image:: ../_static/live_block_viewer.png

`Open this example on GitLab <https://gitlab.com/viperscience/ogmios-python/-/blob/main/examples/live_block_viewer.py>`_
