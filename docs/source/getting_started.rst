Getting Started
===============

Installation
------------

1. Install cardano-node and Ogmios server as described `here <https://ogmios.dev/getting-started/>`_. (Docker installation is recommended.)
2. Install ogmios-python using pip:

``pip install ogmios``

.. image:: _static/ogmios_setup.gif
    :alt: GIF walking through the setup process

Quickstart
----------

To see how easy it is to build on Cardano with ogmios-python, let's use the library to view new blocks as they are added to the blockchain: ::

    import ogmios

    with ogmios.Client() as client:
        # Set chain pointer to origin
        _, tip, _ = client.find_intersection.execute([ogmios.Origin()])

        # Now set chain pointer to tip
        _, _, _ = client.find_intersection.execute([tip.to_point()])

        # Tail blockchain as new blocks come in beyond the current tip
        while True:
            direction, tip, point, _ = client.next_block.execute()
            if direction == ogmios.Direction.forward:
                print(f"New block: {point}")

.. image:: _static/live_block_viewer.png

For more examples, see the Examples section as well as the example scripts in the `repo <https://gitlab.com/viperscience/ogmios-python/-/tree/main/examples>`_.

Library Overview
----------------

The ogmios-python library consists of three main components:

1. A client class for interacting with the Ogmios server
2. A datatypes module for representing the Ogmios data model in a Pythonic way
3. Implementations of the four Ogmios mini-protocols. (For more information, please visit the `Ogmios Mini-Protocols documentation <https://ogmios.dev/mini-protocols/>`_.)

The client class is the main entry point for interacting with the Ogmios server. It provides functions for executing mini-protocol methods. For example, to execute the 
``FindIntersection`` method of the ``ChainSync`` mini-protocol, you would call ``client.find_intersection.execute()``. The client class also provides a context manager 
for automatically closing the connection to the Ogmios server when you are done using it (e.g. ``with ogmios.Client() as client:``).

The datatypes module provides Python classes for representing the Ogmios data model. For example, the ``Point`` class represents a point on the blockchain (slot number and 
block hash), and the ``Utxo`` class represents an unspent transaction output (UTxO). These classes are used to pass data to and from the Ogmios server.

The implementations of the four Ogmios mini-protocols are used internally by the client class. (Although they are also available for use directly if you want to implement
your own client class.) These implementations are based on the `Ogmios mini-protocol specifications <https://ogmios.dev/mini-protocols/>`_ and are designed to be easy to 
use in Python.

See the API section for complete documentation on all of these components.
